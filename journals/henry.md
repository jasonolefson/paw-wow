# Henry's Journal

# October 3rd, 2022

Today we got the starter project and worked on configuring it.
Sorena and Ikenna shared their screens while everyone else
observed and tried to help if they came across any issues.
We came across one issue that stumped us all where the
docker-compose.yaml file needed volumes on the outer level.
Everything was working how it should be by the end of the day.

# October 4, 2022

Today I worked on implementing the Magic Bell notification API.
I installed it onto the ghi container that handles React components
on my gitlab branch. I came across 69 vulnerabilities and tried
running "npm audit fix" and "npm audit fix --force" but those ran
into some errors. I met with the cohort lead, Tai, and told her
one of my goals this module was to ask more questions and I think
we came to a goal of at least 2 questions a week.

# October 5, 2022

America was the driver today and we worked on creating tables in
the database today. I followed along by watching the exploration
videos from this weekend and found it helpful. During individual
work time, I watched a little bit of the videos and was able to
follow along a lot better while America was live coding. America
was very on top of everything and was able to answer the questions
I had. We ran into some issues that were resolved quickly.

# October 6, 2022

After our team's morning meeting we decided to split up the work
for today. I was able to create a characteristics table and endpoints
for creating, deleting, and updating the table. I ran into some issues
with migrations and it turned out to be a file naming issue where I forgot
to put the file extension at the end of the file name. At the end of the
day I ran into merging conflicts that my team and Curtis helped me through.
Today was very productive individually and as a group and I'm happy
with how far the group has gotten. We're a good team.

# October 10, 2022

Today we spent the day working on individual parts of the project.
I worked on integrating the third party notifications API. I spent
a lot of the day reading the documentation and getting the install
of the API working.